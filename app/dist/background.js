const dropboxfuse = {
	fileSystemId: "dropboxfuse",
	displayName: "Dropbox Fuse",
	writeable: false,
	openedFilesLimit: 0,
	supportsNotifyTag: false 
}
chrome.app.runtime.onLaunched.addListener(function() {
  chrome.app.window.create('window.html', {
    'outerBounds': {
      'width': 770,
      'height': 300
    },
    resizable: true
  });
});

chrome.fileSystemProvider.onUnmountRequested.addListener(onUnmountRequested);

var onUnmountRequested = function (options, unmountSuccessful, unmountFailed){
	console.log('onUnmountRequested');
	unmountSuccessful();
	chrome.fileSystemProvider.unmount({fileSystemId: dropboxfuse.fileSystemId});
}