const gulp = require('gulp');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const webpack = require('webpack-stream');

var paths = {
  javascript: './app/src/scripts/*.js',
  sass: './app/src/styles/*.scss',
  html: './app/src/window.html',
  backgroundjs: './app/src/background.js'
};

gulp.task('default', ['bundle', 'sass', 'move', 'backgroundjs', 'watch']);

// gulp.task('javascript', function(){
// 	return gulp.src('./app/src/scripts/*.js')
// 	.pipe(gulp.dest('./app/dist/script/'))
// })

gulp.task('bundle', function() {
  return gulp.src('./app/src/scripts/dropbox.js')
    .pipe(webpack({
      // Any configuration options...
      output: {
          filename: 'dropbox.js',
      }
    
    }))
    .pipe(gulp.dest('./app/dist/script'));
});

gulp.task('sass', function(){
  return gulp.src('./app/src/styles/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('./app/dist/style/'))
});

gulp.task('backgroundjs', function(){
  return gulp.src('./app/src/background.js')
    .pipe(gulp.dest('./app/dist/'))
});

gulp.task('move', function(){
	return gulp.src(['./app/src/manifest.json', './app/src/window.html'])
	.pipe(gulp.dest('./app/dist'))
});

gulp.task('watch', function(){
  gulp.watch(paths.javascript, ['javascript']);
  gulp.watch(paths.sass, ['sass']);
  gulp.watch(paths.html, ['move']);
  gulp.watch(paths.backgroundjs, ['backgroundjs'])
});